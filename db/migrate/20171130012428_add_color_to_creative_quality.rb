class AddColorToCreativeQuality < ActiveRecord::Migration[5.0]
  def change
    add_column :creative_qualities, :color, :integer
  end
end
