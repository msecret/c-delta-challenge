require 'rails_helper'

describe Response do
  context 'associations' do
    it { is_expected.to have_many(:question_responses) }
  end

  context 'validations' do
    it { is_expected.to validate_presence_of :first_name }
    it { is_expected.to validate_presence_of :last_name }
  end

  describe '#display_name' do
    let(:response) { Response.new(first_name: 'Tal', last_name: 'Safran') }

    it 'concatenates the first and last name' do
      expect(response.display_name).to eql('Tal Safran')
    end
  end

  describe '#completed?' do
    let(:response) { Response.new }

    before do
      allow(Question).to receive(:count).and_return(3)
      allow(response).to receive_message_chain(:question_responses, :count)
        .and_return(response_count)
    end

    context 'when no responses exist' do
      let(:response_count) { 0 }
      it 'is false' do
        expect(response.completed?).to be(false)
      end
    end

    context 'when some responses exist' do
      let(:response_count) { 1 }
      it 'is false' do
        expect(response.completed?).to be(false)
      end
    end

    context 'when responses exist for all questions' do
      let(:response_count) { 3 }
      it 'is true' do
        expect(response.completed?).to be(true)
      end
    end
  end

  describe '#responses_by_creative_quality' do
    let(:response) { build :response }
    let(:creative_quality) { build :creative_quality }

    before do
      question_choice = QuestionChoice.new(
        creative_quality: creative_quality)
      allow(response).to receive(:question_responses).and_return(
        [FactoryGirl.build(:question_response, question_choice: question_choice)])
    end

    it 'should return one response filtered by creative quality name' do
      expect(response.responses_by_creative_quality(creative_quality).count).to eql(1);
    end
  end

  describe '#scores_by_creative_quality' do
    let(:response) { Response.new }

    before do
      question_choice = QuestionChoice.new(
        score: 5,
        creative_quality: CreativeQuality.new(name: 'cqa')
      )
      qr = QuestionResponse.new(
        response: response,
        question_choice: question_choice
      )
      allow(response).to receive(:responses_by_creative_quality).and_return(
        [qr])
    end

    it 'should return a list of scores' do
      expect(response.scores_by_creative_quality(
        CreativeQuality.new).count).to eql(1);
      expect(response.scores_by_creative_quality(
        CreativeQuality.new)[0]).to eql(5);
    end
  end

  describe '#norm_by_creative_quality' do
    let(:response) { Response.new }

    before do
      allow(response).to receive(:scores_by_creative_quality).and_return(
        [2, 2, 2])
      allow(response).to receive(:choice_scores_by_creative_quality).and_return(
        [[1, 2, 3], [1, 2, 3], [1, 2, 3]])
    end

    it 'should return a normalized with each question response' do
      expect(response.norm_by_creative_quality(CreativeQuality.new)).to eql(75);
    end
  end

  describe '#min_by_creative_quality' do
    let(:response) { Response.new }

    before do
      allow(response).to receive(:choice_scores_by_creative_quality).and_return(
        [[1, 2, 3], [-1, 2, 3], [1, 2, 3]])
    end

    it 'should return a normalized with each question response' do
      expect(response.min_by_creative_quality(CreativeQuality.new)).to eql(1);
    end
  end

  describe '#max_by_creative_quality' do
    let(:response) { Response.new }

    before do
      allow(response).to receive(:choice_scores_by_creative_quality).and_return(
        [[1, 2, 3], [-1, 2, 3], [1, 2]])
    end

    it 'should return a normalized with each question response' do
      expect(response.max_by_creative_quality(CreativeQuality.new)).to eql(8);
    end
  end
end
