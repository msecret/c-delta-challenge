require 'rails_helper'

describe CreativeQuality do
  context 'validations' do
    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_presence_of(:description) }
    it { should define_enum_for(:color).with(
      [:primary, :danger, :success, :info, :warning]) }
    it { is_expected.to validate_presence_of(:color) }
  end

  context 'callbacks' do
    before (:each) do
      @cq = CreativeQuality.create
    end

    it 'always adds a color' do
      expect(@cq.color).not_to be_nil
    end

    it 'always adds a unique color' do
      cq2 = CreativeQuality.new
      expect(cq2.color).not_to eq(@cq.color)
    end
  end
end
