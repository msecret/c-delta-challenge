class CreativeQuality < ApplicationRecord
  validates :name, presence: true
  validates :description, presence: true
  enum color: {
    primary: 0,
    danger: 1,
    success: 2,
    info: 3,
    warning: 4
  }
  validates :color, uniqueness: true, presence: true

  after_initialize :init_color

  def init_color
    if self.color.blank?
      potential_colors = (0..4).to_a
      existing_colors = CreativeQuality.all.map{ |c| c.color }
      free_colors = potential_colors.select{ |cdx| existing_colors.exclude?(cdx) }
      self.color = free_colors.sample
    end
  end
end
