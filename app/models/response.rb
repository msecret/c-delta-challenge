class Response < ApplicationRecord
  has_many :question_responses

  validates :first_name, presence: true
  validates :last_name, presence: true

  delegate :count, to: :question_responses, prefix: true

  def display_name
    "#{first_name} #{last_name}"
  end

  def completed?
    question_responses_count == Question.count
  end

  def responses_by_creative_quality(creative_quality)
    self.question_responses.select do |question_response|
      question_response.question_choice.creative_quality ==
        creative_quality
    end
  end

  def scores_by_creative_quality(creative_quality)
    self.responses_by_creative_quality(creative_quality).map {
      |question_response| question_response.question_choice.score
    }
  end

  def norm_by_creative_quality(creative_quality)
    raw = self.scores_by_creative_quality(creative_quality).reduce(0, :+)
    min = self.min_by_creative_quality(creative_quality)
    max = self.max_by_creative_quality(creative_quality)
    self.normalize_score(raw, min, max)
  end

  def min_by_creative_quality(creative_quality)
    total_choices = self.choice_scores_by_creative_quality(
      creative_quality)
    total_choices.reduce(0) {|acc, choices| acc + choices.min}
  end

  def max_by_creative_quality(creative_quality)
    total_choices = self.choice_scores_by_creative_quality(
      creative_quality)
    total_choices.reduce(0) {|acc, choices| acc + choices.max}
  end

  protected
  def choice_scores_by_creative_quality(creative_quality)
    totals = []
    self.responses_by_creative_quality(creative_quality).each {
      |question_response|
      total_q_r = []
      question_response.question.choices.each { |choice|
        total_q_r.push(choice.score)
      }
      totals.push(total_q_r)
    }
    return totals
  end

  def normalize_score(raw_score, min_score, max_score)
    raw = raw_score.to_f
    min = min_score.to_f
    max = max_score.to_f
    ((min.abs + raw) / (min.abs + max) * 100.0).round
  end
end
